#!/usr/bin/env python

# script to generate comparison SSIM values across a range
# of files, bitrates and speedlevels

import subprocess
from threading import Thread
from Queue import Queue
from os.path import basename
from glob import glob
from operator import itemgetter
import logging

ENCODE = 'examples/encoder_example'
DUMP_SSIM = '../derf-tools/dump_ssim'
OGGINFO = 'ogginfo'
Y4MDIR = '../y4m'
THREADS = 4

bitrates = [2**x for x in xrange(4,11)] + \
           [320*x for x in xrange(1,7)] + \
           [2048*x for x in xrange(1,5)]

# call libtheora_info to get the maximum speed level, if it exists
try:
  lines = subprocess.check_output(['examples/libtheora_info'])
  for line in lines.split('\n'):
    if 'Maximum speed' in line:
      maxspeed = int(line.split()[-1])
except:
  maxspeed = 2
speeds = range(maxspeed + 1)

# set the default logging level
logging.getLogger().setLevel(logging.DEBUG)

def basefile(infile):
  '''Generate a base filename to use as a data key'''
  return '.'.join(basename(infile).split('.')[:-1])

def ogg_file(infile, bitrate=1024, speed=1):
  '''Generate a ogg filename for the given inputs'''
  base = basefile(infile)
  return base + '-V' + str(bitrate) + '-z' + str(speed) + ".ogv"

def encode_job(y4mfile, target=1024, speed=1):
  '''Return a job dictionary with particular encoding parameters.'''
  base = basefile(y4mfile)
  oggfile = ogg_file(y4mfile, target, speed)
  job = {'base': base, 'target': target, 'speed': speed,
         'y4mfile': y4mfile, 'oggfile': oggfile}
  return job

def worker(queue, results):
  '''Worker function. These run in their own thread, running
     any jobs they find submitted to the queue.'''
  while True:
    # block until we can pull a job from the queue
    job = queue.get()
    try:
      logging.debug(' '.join(map(str,job.values())))

      # encode the file
      cmd = [ENCODE, '-q', '-V', job['target'], '-z', job['speed'],
                     '-o', job['oggfile'], job['y4mfile']]
      cmd = map(str, cmd)
      logging.debug(' '.join(cmd))
      result = subprocess.check_output(cmd)

      # measure the bitrate
      cmd = [OGGINFO, job['oggfile']]
      logging.debug(' '.join(cmd))
      result = subprocess.check_output(cmd)
      for line in result.split('\n'):
        if 'Average bitrate:' in line:
          bitrate = float(line.split()[-2])
      job['bitrate'] = bitrate
      logging.debug(' '.join(['ogginfo', job['oggfile'], str(bitrate), 'kbps']))

      # measure the SSIM
      cmd = [DUMP_SSIM, '-s', job['oggfile'], job['y4mfile']]
      logging.debug(' '.join(cmd))
      result = subprocess.check_output(cmd)
      for line in result.split('\n'):
        if 'Total:' in line:
          row = line.split()
          job['ssim'] = float(row[1])
          job['luma'] = float(row[3])
          job['cb'] = float(row[5])
          job['cr'] = float(row[7])
      logging.debug(' '.join(['ssim', job['oggfile'], str(job['ssim']), 'dB']))

      # save the results
      results.put(job)

      # remove the file
      cmd = ['rm', job['oggfile']]
      logging.debug(' '.join(cmd))
      result = subprocess.check_output(cmd)

    finally:
      queue.task_done()

def writer(queue, results):
  '''Data summary function.
     This runs in its own thread and prints out data from completed
     jobs as it becomes available.'''
  print "# file\tspeed_level\ttarget_kbps\treal_kbps\tTotal\tLuma\tCb\tCr"
  while True:
    job = results.get()
    try:
        result = [job[x] for x in 'base', 'speed', 'target', 'bitrate',
                                  'ssim', 'luma', 'cb', 'cr']
        print '\t'.join( map(str, result) )
    finally:
      results.task_done()


if __name__ == '__main__':
  print "# libtheora SSIM scores"
  print "# running bitrates", bitrates
  print "# speed levels", speeds
  
  # create queues and worker threads
  logging.info('creating queues and workers...')
  queue = Queue()
  results = Queue()
  for _ in xrange(THREADS):
    w = Thread(target=worker,args=(queue,results))
    w.daemon = True
    w.start()
  w = Thread(target=writer,args=(queue,results))
  w.daemon = True
  w.start()

  logging.info('generating jobs...')

  # testing: use a single, small file by default
  infiles = glob(Y4MDIR + '/akiyo_qcif.y4m')
  #infiles = glob('/home/giles/Videos/sintel/sintel_trailer_2k_720p24.y4m')
  # full run: all the base (non-HD) y4m files
  #infiles = glob(Y4MDIR + '/*.y4m')
  # strip interlaced files
  infiles = filter(lambda item: '_i_' not in item, infiles)

  # generate jobs
  for infile in infiles:
    for bitrate in bitrates:
      for speed in speeds:
        job = encode_job(infile, bitrate, speed)
        queue.put(job)

  # clean up
  logging.info('waiting for jobs to complete...')
  queue.join()
  results.join()
