#!/usr/bin/env python

from math import sqrt

def read_data(file):
  data = {}
  f = open(file)
  for row in f.readlines():
    if row[0] == '#':
      continue
    try:
      frames, speed, time, rate = row.split()
    except ValueError:
      pass
    speed = int(speed)
    frames = int(frames)
    if not speed in data:
      data[speed] = []
    data[speed].append(map(float,(time, rate)))
  return data

def avg_data(data):
  out = {}
  for speed in data.keys():
    # compute the average
    avg_time = 0.0
    avg_rate = 0.0
    n = len(data[speed])
    for row in data[speed]:
      avg_time += row[0]
      avg_rate += row[1]
    avg_time /= n
    avg_rate /= n
    # now the deviation
    dev_time = 0.0
    dev_rate = 0.0
    for row in data[speed]:
      dev_time += (row[0] - avg_time)**2
      dev_rate += (row[1] - avg_rate)**2
    dev_time = sqrt(dev_time/(n-1))
    dev_rate = sqrt(dev_rate/(n-1))
    out[speed] = [avg_time, dev_time, avg_rate, dev_rate]
  return out

def print_avg(data):
  headings = ['speed','seconds avg', 'seconds dev', 'Mpixel/s avg', 'Mpixel/s dev']
  print '# ' + '\t'.join(headings)
  for speed in data.keys():
    avg_time, dev_time, avg_rate, dev_rate = data[speed]
    print '\t'.join(map(str,(speed, avg_time, dev_time, avg_rate, dev_rate)))


if __name__ == '__main__':
  from sys import argv,exit
  if len(argv) != 2:
    print 'usage:', argv[0], '<data.perf>'
    print ' reads a file of tab-separated index, time, rate lines'
    print ' and prints averages and standard deviations for each index.'
    exit(1)
  data = read_data(argv[1])
  avg = avg_data(data)
  print_avg(avg)
