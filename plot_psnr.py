#!/usr/bin/env python

# Quick script to plot psnr scores for two files against each other

import subprocess
from matplotlib import pyplot

DUMP_PSNR = 'examples/dump_psnr'

def print_data(data):
  print '# frame\tPSNR\tLuma\tCb\tCr'
  for row in data:
    print '\t'.join(map(str,row))

def plot_data(data, label=None):
  frames = [row[0] for row in data]
  psnr = [row[1] for row in data]
  pyplot.plot(frames, psnr, label=label)
  pyplot.ylabel('PSNR (db)')
  pyplot.xlabel('frames')
  pyplot.legend()

def read_data(y4mfile, oggfile):
  result = subprocess.check_output([DUMP_PSNR, y4mfile, oggfile])
  data = []

  for line in result.split('\n'):
    if 'Total' in line: continue
    try:
      fields = line.split()
      frame = int(fields[0][:-1])
      psnr = float(fields[1])
      Y = float(fields[3])
      Cb = float(fields[5])
      Cr = float(fields[7][:-1])
      data.append((frame, psnr, Y, Cb, Cr))
    except IndexError:
      pass

  return data

y4mfile = '../y4m/akiyo_qcif.y4m'
pyplot.title('Encoding comparison for ' + y4mfile)
for file in ['akiyo_qcif-V512-z1.ogv', 'akiyo_qcif-V512-z3.ogv']:
  data = read_data(y4mfile, file)
  plot_data(data, file)
pyplot.show()

