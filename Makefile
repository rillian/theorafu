# GNU makefile

PROGS := theorafu

CFLAGS += -g -Wall

CFLAGS += -I$(HOME)/projects/theora/include
LIBS += $(HOME)/projects/theora/lib/libtheoraenc.a
LIBS += $(HOME)/projects/theora/lib/libtheoradec.a -logg

CFLAGS += $(shell pkg-config --cflags gtk+-2.0)
LIBS += $(shell pkg-config --libs gtk+-2.0)

all: $(PROGS)

check: all

clean:
	$(RM) *.o
	$(RM) $(PROGS)

dist:
	@echo "dist target not yet implemented"

.PHONY: all check clean dist

theorafu: theorafu.o
	$(CC) $(LDFLAGS) -o $@ $^ $(LIBS) 
