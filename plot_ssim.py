#!/usr/bin/env python

'''Read and plot ssim data runs'''

from matplotlib import pyplot

def read_data(name):
  '''Read a tab-separated data file, returning a dictionary.
     The expected format is:
     filename, rate (kbps), speed level, total SSIM, Luma, Cb, Cr
  '''

  data = {}

  file = open(name)
  for line in file.readlines():
    # skip comments
    if line[0] == '#':
      continue

    # grab each field
    base, speed, target, rate, ssim, luma, cb, cr = line.split()
    speed = int(speed)
    target = int(target)
    rate, ssim, luma, cb, cr = map(float, (rate, ssim, luma, cb, cr))
    if base not in data:
      data[base] = {}
    if speed not in data[base]:
      data[base][speed] = {}
    data[base][speed][target] = (rate, ssim, luma, cb, cr)

  return data

def print_data(data):
  '''Print a data dictionary in sorted form.'''
  print '\t'.join( ('#','file', 'speed level', 'target (kbps)', 'rate (kbps)',
                    'SSIM', 'Luma', 'Cb', 'Cr') )
  files = data.keys()
  files.sort()
  for file in files:
    speeds = data[file].keys()
    speeds.sort()
    for speed in speeds:
      rates = data[file][speed].keys()
      rates.sort()
      for target in rates:
        print '\t'.join(map(str, ((file, speed, target) + data[file][speed][target])))
      print

def plot_data(data):
  '''Plot each ssim run in turn.'''
  files = data.keys()
  files.sort()
  for file in files:
    pyplot.title(file)
    pyplot.ylabel('SSIM')
    pyplot.xlabel('target bitrate (kbps)')
    pyplot.ylim( (0,48) )
    speeds = data[file].keys()
    speeds.sort()
    for speed in speeds:
      rates = data[file][speed].keys()
      rates.sort()
      x = [ data[file][speed][target][0] for target in rates ]
      y = [ data[file][speed][target][1] for target in rates ]
      pyplot.plot(x, y, label='speed level ' + str(speed))
    pyplot.legend(loc='lower right')
    png = file + '.png'
    print 'saving', png
    pyplot.savefig(png)
    pyplot.close()
    #pyplot.show()

def html_data(name):
  import os.path
  data = read_data(name)
  base = os.path.basename(name)
  plot_data(data)
  html = base + '.html'
  print 'saving', html
  file = open(html, 'w')
  file.write('<!DOCTYPE HTML>\n')
  file.write('<html>\n')
  file.write('<head><title>' + base + ' graphs</title></head>\n')
  file.write('<body>\n')
  file.write('<h1>SSIM plots for ' + base + '</h1>\n\n')
  file.write('<p>Raw data: <a href="' + name + '">' + base + '</a></p>\n')
  files = data.keys()
  files.sort()
  for graph in files:
    png = graph + '.png'
    file.write('  <img src="' + png + '" alt="' + graph + ' ssim graph">\n')
  file.write('</body>\n')
  file.write('</html>\n')

if __name__ == '__main__':
  from sys import argv,exit
  if len(argv) != 2:
    print 'usage:', argv[0], '<data.ssim>'
    print ' reads a file of tab-separated values. Expected format is:'
    print '\t'.join( ('  ','file', 'speed level', 'target', 'bitrate',
                           'SSIM', 'Luma', 'Cb', 'Cr') )
    exit(1)
  html_data(argv[1])
