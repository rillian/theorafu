/* theora quality/performance benchmarking */

#include <gtk/gtk.h>

#include <stdlib.h>
#include <stdio.h>

#include <theora/theoradec.h>
#include <theora/theoraenc.h>

/* the the maximum supported value for the 'speedlevel' knob.
   if the passed th_enc_ctx is null, we construct our own dummy */
int get_max_speedlevel(th_enc_ctx *_th) {
  th_info ti;
  th_enc_ctx *th;
  int max_speedlevel = -1;
  int ret;

  if (_th == NULL) {
    /* allocate a dummy encoder context */
    th_info_init(&ti);
    ti.frame_width = 320;
    ti.frame_height = 240;
    th = th_encode_alloc(&ti);
  } else {
    /* use the caller's supplied context */
    th = _th;
  }

  ret = th_encode_ctl(th, TH_ENCCTL_GET_SPLEVEL_MAX,
                      &max_speedlevel, sizeof(int));
  if (ret) {
    fprintf(stderr, "error: th_encode_ctl returned %d in %s\n", ret, __FUNCTION__);
  }

  if (_th == NULL) {
    /* clean up our dummy encoder context */
    th_encode_free(th);
    th_info_clear(&ti);
  }

  return max_speedlevel;
}

int main(int argc, char *argv[])
{
  GtkWidget *window;
  GtkWidget *box, *column, *slider, *label;
  int max_speedlevel;

  gtk_init(&argc,&argv);

  window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title(GTK_WINDOW(window), argv[0]);

  g_signal_connect(window, "delete_event", G_CALLBACK(gtk_main_quit), NULL);
  g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);

  box = gtk_hbox_new(FALSE, 0);
  gtk_container_add(GTK_CONTAINER(window), box);

  column = gtk_vbox_new(FALSE, 0);
  label = gtk_label_new("bitrate");
  gtk_box_pack_start(GTK_BOX(column), label, FALSE, TRUE, 0);
  gtk_widget_show(label);

  slider = gtk_vscale_new_with_range(0, 1000, 1);
  gtk_scale_set_value_pos(slider, GTK_POS_BOTTOM);
  gtk_box_pack_start(GTK_BOX(column), slider, TRUE, TRUE, 0);
  gtk_widget_show(slider);

  gtk_box_pack_start(GTK_BOX(box), column, TRUE, TRUE, 0);
  gtk_widget_show(column);

  column = gtk_vbox_new(FALSE, 0);
  label = gtk_label_new("quality");
  gtk_box_pack_start(GTK_BOX(column), label, FALSE, TRUE, 0);
  gtk_widget_show(label);

  slider = gtk_vscale_new_with_range(0, 10, 1);
  gtk_scale_set_value_pos(slider, GTK_POS_BOTTOM);
  gtk_box_pack_start(GTK_BOX(column), slider, TRUE, TRUE, 0);
  gtk_widget_show(slider);

  gtk_box_pack_start(GTK_BOX(box), column, TRUE, TRUE, 0);
  gtk_widget_show(column);

  max_speedlevel = get_max_speedlevel(NULL);
  if (max_speedlevel < 0)
    max_speedlevel = 0;
  fprintf(stderr, "max_speedlevel %d\n", max_speedlevel);

  column = gtk_vbox_new(FALSE, 0);
  label = gtk_label_new("speed");
  gtk_box_pack_start(GTK_BOX(column), label, FALSE, TRUE, 0);
  gtk_widget_show(label);

  slider = gtk_vscale_new_with_range(0, max_speedlevel, 1);
  gtk_scale_set_value_pos(slider, GTK_POS_BOTTOM);
  gtk_box_pack_start(GTK_BOX(column), slider, TRUE, TRUE, 0);
  gtk_widget_show(slider);

  gtk_box_pack_start(GTK_BOX(box), column, TRUE, TRUE, 0);
  gtk_widget_show(column);

  gtk_widget_show(box);

  gtk_widget_show(window);

  gtk_main();

  return 0;
}
